﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance = null;
    public AudioSource bgAudio;
    public AudioSource uiAudio;
    public AudioSource toolAudio;
    // Start is called before the first frame update
    public void Init()
    {
        Instance = this;
        bgAudio.volume = 0.5f;
        uiAudio.volume = 0.5f;
        Debug.Log("Init AudioManager...");
    }

    /// <summary>
    /// 播放背景音乐
    /// </summary>
    /// <param name="name"></param>
    /// <param name="isLoop"></param>
    public void PlayBGMusic(string name, bool isLoop = true)
    {
        AudioClip audio = ResManager.Instance.LoadAudio("Audio/BGM/" + name, true);
        if (bgAudio.clip == null || bgAudio.clip.name != audio.name)
        {
            bgAudio.clip = audio;
            bgAudio.loop = isLoop;
            bgAudio.Play();
        }
    }
    /// <summary>
    /// UI操作音效
    /// </summary>
    public void PlayUIAudio(string name)
    {
        AudioClip audio = ResManager.Instance.LoadAudio("Audio/SoundEffect/" + name, true);
        uiAudio.clip = audio;
        uiAudio.Play();
    }
    /// <summary>
    /// tool音效播放
    /// </summary>
    public void PlayToolAudio(string name)
    {
        AudioClip audio = ResManager.Instance.LoadAudio("Audio/SoundEffect/" + name, true);
        toolAudio.clip = audio;
        toolAudio.Play();
    }
    /// <summary>
    /// tool音效暂停
    /// </summary>
    public void PauseToolAudio(string name)
    {
        AudioClip audio = ResManager.Instance.LoadAudio("Audio/SoundEffect/" + name, true);
        toolAudio.clip = audio;
        toolAudio.Pause();
    }
}
