﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ValueManager : MonoBehaviour
{
    public static ValueManager Instance = null;
    private UIManager uiManager = null;
    [SerializeField]
    private float player1Score = 0;
    [SerializeField]
    private float player2Score = 0;

    [SerializeField]
    private int player1WinCount =0;
    [SerializeField]
    private int player2WinCount = 0;

    public bool isLocked = true;
    public void Init()
    {
        Instance = this;
        uiManager = UIManager.Instance;
        Debug.Log("Init ValueManager...");
    }

    public void UpdatePlayer1Score(float value)
    {    
        if (player1Score>=40)
        {
            player1Score = 0;
            player2Score = 0;
        }
        player1Score += value;
    }

    public void UpdatePlayer2Score(float value)
    {
        if (player2Score >= 40)
        {
            player1Score = 0;
            player2Score = 0;
        }
        player2Score += value;
    }

    public float GetPlayer1Score()
    {
        return player1Score;
    }
    public float GetPlayer2Score()
    {
        return player2Score;
    }

    public int GetPlayer1Wincount()
    {
        return player1WinCount;
    }
    public int GetPlayer2Wincount()
    {
        return player2WinCount;
    }

    public void Play1Win()
    {
        player1WinCount++;
    }

    public void Play2Win()
    {
        player2WinCount++;
    }

    //若回到主菜单，清空游戏分数
    public void ClearWinScore()
    {
        player1Score = 0;
        player2Score = 0;
    }
}
