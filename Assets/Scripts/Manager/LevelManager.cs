﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour 
{
    public static LevelManager Instance = null;
    private UIManager uiManager;
    private int gameTime=180;
    public void Init()
    {
        Instance = this;
        uiManager = UIManager.Instance;
        Debug.Log("Init LevelManager...");
    }
    #region 场景加载
    /// <summary>
    /// 加载下一个场景
    /// </summary>
    public void NextScence()
    {
        switch (GetActiveSceneName())
        {
            case "Level1":
                LoadLevel2(gameTime);
                break;
            case "Level2":
                LoadLevel3(gameTime);
                break;
            case "Level3":
                LoadLevel4(gameTime);
                break;
            case "Level4":
                LoadMenu();
                break;
            case "Menu":
                LoadLevel1(gameTime);
                break;
            default:
                break;
        }
    }
    /// <summary>
    /// 加载主菜单
    /// </summary>
    public void LoadMenu()
    {
        //Play BGM
        AudioManager.Instance.PlayBGMusic("O1", true);

        //清空当前游戏分数
        ValueManager.Instance.ClearWinScore();

        ResManager.Instance.ASyncLoadScene("Menu", ()=>
        {
            uiManager.menuWnd.SetWindSate(true);
            uiManager.gameWnd.SetWindSate(false);
            uiManager.chooseWnd.SetWindSate(false);
        });
        
    }
    /// <summary>
    /// 加载第一关
    /// </summary>
    public void LoadLevel1(int time =180)
    {
        uiManager.gameWnd.TotalTime = time;
        this.gameTime = time;
        //Play BGM
        AudioManager.Instance.PlayBGMusic("G1", true);

        ResManager.Instance.ASyncLoadScene("Level1", () =>
        {
            uiManager.menuWnd.SetWindSate(false);
            uiManager.enterWnd.SetWindSate(true);
            uiManager.chooseWnd.SetWindSate(false);
        });
    }
    /// <summary>
    /// 加载第二关
    /// </summary>
    public void LoadLevel2(int time = 180)
    {
        uiManager.gameWnd.TotalTime = time;
        this.gameTime = time;
        //Play BGM
        AudioManager.Instance.PlayBGMusic("G2", true);

        ResManager.Instance.ASyncLoadScene("Level2", () =>
        {
            uiManager.menuWnd.SetWindSate(false);
            uiManager.enterWnd.SetWindSate(true);
            uiManager.chooseWnd.SetWindSate(false);
        });
    }
    /// <summary>
    /// 加载第三关
    /// </summary>
    public void LoadLevel3(int time = 180)
    {
        uiManager.gameWnd.TotalTime = time;
        this.gameTime = time;
        //Play BGM
        AudioManager.Instance.PlayBGMusic("G3", true);

        ResManager.Instance.ASyncLoadScene("Level3", () =>
        {
            uiManager.menuWnd.SetWindSate(false);
            uiManager.enterWnd.SetWindSate(true);
            uiManager.chooseWnd.SetWindSate(false);
        });
    }
    /// <summary>
    ///  加载第四关
    /// </summary>
    public void LoadLevel4(int time = 180)
    {
        uiManager.gameWnd.TotalTime = time;
        this.gameTime = time;
        //Play BGM
        AudioManager.Instance.PlayBGMusic("G4", true);

        ResManager.Instance.ASyncLoadScene("Level4", () =>
        {
            uiManager.menuWnd.SetWindSate(false);
            uiManager.enterWnd.SetWindSate(true);
            uiManager.chooseWnd.SetWindSate(false);
        });
    }

    public void LoadBonusLevel1(int time = 180)
    {
        uiManager.gameWnd.TotalTime = time;
        this.gameTime = time;
        //Play BGM
        AudioManager.Instance.PlayBGMusic("J1", true);

        ResManager.Instance.ASyncLoadScene("BonusLevel1", () =>
        {
            uiManager.menuWnd.SetWindSate(false);
            uiManager.enterWnd.SetWindSate(true);
            uiManager.chooseWnd.SetWindSate(false);
        });
    }

    public void LoadBonusLevel2(int time = 180)
    {
        uiManager.gameWnd.TotalTime = time;
        this.gameTime = time;
        //Play BGM
        AudioManager.Instance.PlayBGMusic("J2", true);

        ResManager.Instance.ASyncLoadScene("BonusLevel2", () =>
        {
            uiManager.menuWnd.SetWindSate(false);
            uiManager.enterWnd.SetWindSate(true);
            uiManager.chooseWnd.SetWindSate(false);
        });
    }

    public void LoadBonusLevel3(int time = 180)
    {
        uiManager.gameWnd.TotalTime = time;
        this.gameTime = time;
        //Play BGM
        AudioManager.Instance.PlayBGMusic("J3", true);

        ResManager.Instance.ASyncLoadScene("BonusLevel3", () =>
        {
            uiManager.menuWnd.SetWindSate(false);
            uiManager.enterWnd.SetWindSate(true);
            uiManager.chooseWnd.SetWindSate(false);
        });
    }

    public void LoadBonusLevel4(int time = 180)
    {
        uiManager.gameWnd.TotalTime = time;
        this.gameTime = time;
        //Play BGM
        AudioManager.Instance.PlayBGMusic("J4", true);

        ResManager.Instance.ASyncLoadScene("BonusLevel4", () =>
        {
            uiManager.menuWnd.SetWindSate(false);
            uiManager.enterWnd.SetWindSate(true);
            uiManager.chooseWnd.SetWindSate(false);
        });
    }
    #endregion

    /// <summary>
    /// 获取当前关卡的名字
    /// </summary>
    /// <returns></returns>
    public string GetActiveSceneName()
    {
        return SceneManager.GetActiveScene().name;
    }
    /// <summary>
    /// 获取当前关卡的buildIndex
    /// </summary>
    /// <returns></returns>
    public int GetActiveSceneIndex()
    {
        return SceneManager.GetActiveScene().buildIndex;
    }
}