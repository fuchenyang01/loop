﻿using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour 
{
    public MenuWnd menuWnd;
    public GameWnd gameWnd;
    public SetWnd setWnd;
    public ControlWnd controlWnd;
    public AchievementWnd achievementWnd;
    public PauseWnd pauseWnd;
    public CheckoutWnd checkoutWnd;
    public EnterWnd enterWnd;
    public ChooseWnd chooseWnd;
    public StoryWnd storyWnd;
    public NameListWnd nameListWnd;
    public static UIManager Instance = null;
    private LevelManager levelManager = null;

    public void Init()
    {
        Instance = this;
        levelManager = LevelManager.Instance;
        Debug.Log("Init UIManager...");
    }
}