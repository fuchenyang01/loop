﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ButtonEvent : MonoBehaviour
{
    /// <summary>
    /// 未选中时的图标
    /// </summary>
    public Sprite unSelected;
    /// <summary>
    /// 选中时的图标
    /// </summary>
    public Sprite selected;
    public float selectedW = 223;
    public float selectedH = 52;
    public GameObject imgSelected;
    private float W;
    private float H;
    private Image image;
    private void Start()
    {
        image = GetComponent<Image>();
        W = GetComponent<RectTransform>().rect.width;
        H = GetComponent<RectTransform>().rect.height;

    }
    /// <summary>
    /// 鼠标进入
    /// </summary>
    public void MouseEnter()
    {
        image.sprite = selected;
        GetComponent<RectTransform>().sizeDelta = new Vector2(selectedW, selectedH);
        imgSelected.SetActive(true);
        imgSelected.transform.position = this.transform.position;
    }
    /// <summary>
    /// 鼠标滑出
    /// </summary>
    public void MouseExit()
    {
        image.sprite = unSelected;
        GetComponent<RectTransform>().sizeDelta = new Vector2(W, H);
        imgSelected.SetActive(false);
    }
}
