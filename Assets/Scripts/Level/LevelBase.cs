﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelBase : MonoBehaviour
{
    //使者地面的sprite
    public Sprite[] sprite_elpis;
    //灾厄地面的sprite
    public Sprite[] sprite_disaster;
    //不变地地面的sprite
    public Sprite[] sprite_wall;
    /// <summary>
    /// 道具prefabs
    /// </summary>
    public GameObject[] ToolCell;

    /// <summary>
    /// 空地
    /// </summary>
    public GameObject space;
    /// <summary>
    /// 变化的方块
    /// </summary>
    public GameObject variable_wall;
    /// <summary>
    /// 不变的方块
    /// </summary>
    public GameObject wall;
    public GameObject Player1; //玩家1
    public GameObject Player2; //玩家2
    /// <summary>
    /// 储存地图中的绿地
    /// </summary>
    public List<GameObject> grass;
    /// <summary>
    /// 储存地图中的废墟
    /// </summary>
    public List<GameObject> wasteland;
    /// <summary>
    /// 地图数组
    /// </summary>
    public int[,] levelMap;
    /// <summary>
    /// 道具数组
    /// </summary>
    public int[,] toolPos;
    /// <summary>
    /// 单向地图数组
    /// </summary>
    public int[,] unidirectionalWallPos;
    /// <summary>
    /// 方块变化特效
    /// </summary>
    public GameObject particalS2Z, particalZ2S;
    /// <summary>
    /// 使者位置
    /// </summary>
    public Vector3 ElpisPos;
    /// <summary>
    /// 灾厄位置
    /// </summary>
    public Vector3 DisasterPos;

    protected void InitMap()
    {
        for (int i = 0; i < levelMap.GetLength(0); i++)
        {
            for (int j = 0; j < levelMap.GetLength(1); j++)
            {
                int ranWall = Random.Range(0, sprite_wall.Length);
                int ranVariable_wall = Random.Range(0, sprite_disaster.Length);
                GameObject go = null;             
                switch (levelMap[i, j])
                {
                    case 0:
                        go = Instantiate(space);
                        break;
                    case 1:
                        go = Instantiate(wall);
                        go.GetComponent<SpriteRenderer>().sprite = sprite_wall[ranWall];
                        break;
                    case 2:
                        go = Instantiate(variable_wall);
                        go.GetComponent<SpriteRenderer>().sprite = sprite_disaster[ranVariable_wall];
                        break;
                }
                if (levelMap[i, j]==2)
                {
                    go.tag = "Disaster";
                    wasteland.Add(go);
                }
                //单向地图
                for (int k = 0;k<unidirectionalWallPos.GetLength(0);k++)
                {
                    if(j == unidirectionalWallPos[k,0] && levelMap.GetLength(0) - i - 3 == unidirectionalWallPos[k,1])
                    {
                        go.AddComponent<PlatformEffector2D>();
                        go.GetComponent<BoxCollider2D>().usedByEffector = true;
                        go.GetComponent<PlatformEffector2D>().useColliderMask = false;
                    }
                }
                if (go != null)
                {
                    go.transform.position = new Vector3(j, levelMap.GetLength(0) - i - 1, 0);
                    go.transform.SetParent(this.transform);
                }

            }
        }
    }

    //初始化player1
    protected void InitPlayer1()
    {
        GameObject player1 = null;
        player1 = Instantiate(Player1);
        if (player1 != null)
        {
            player1.transform.localPosition = ElpisPos;
        }
    }

    //初始化player2
    protected void InitPlayer2()
    {
        GameObject player2 = null;
        player2 = Instantiate(Player2);
        if (player2 != null)
        {
            player2.transform.localPosition = DisasterPos;
        }
    }

    //初始化道具tools
    protected void InitTools()
    {
        GameObject GO = new GameObject("ToolsMap");
        GO.transform.SetParent(this.transform.parent);
        
        for (int i=0;i<toolPos.GetLength(0);i++)
        {

            GameObject tool;
            
            if(toolPos[i,0] == 0)
            {
                //greenplant
                GameObject gp = new GameObject("GreenPlant");
                gp.AddComponent<GreenPlant>();
                gp.transform.SetParent(GO.transform);
                for (int j=0;j<toolPos[i,3];j++)
                {
                    tool = Instantiate(ToolCell[toolPos[i, 0]]);
                    tool.transform.localPosition = new Vector3(toolPos[i, 1], toolPos[i, 2] + 0.25f - j * 0.5f, 0);
                    if (toolPos[i, 3] > 54)
                    {
                        tool.transform.localPosition += new Vector3(0.5f, 0, 0);
                    }
                    tool.transform.SetParent(gp.transform);
                }
            }
            else
            {
                tool = Instantiate(ToolCell[toolPos[i, 0]]);
                tool.AddComponent<Lithops>();
                tool.transform.localPosition = new Vector3(toolPos[i, 1], toolPos[i, 2] - 0.125f, 0);
                tool.transform.SetParent(GO.transform);
            }
        }
    }

    
}
