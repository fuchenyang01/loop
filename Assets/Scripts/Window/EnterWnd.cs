﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnterWnd : WindowRoot
{
    public GameObject[] level;
    private Animator cameraAnimation;
    protected override void InitWnd()
    {
        base.InitWnd();
        cameraAnimation = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Animator>();
        cameraAnimation.Play("MainCamera");
        uIManager.gameWnd.SetWindSate(false);
        //设置时间静止
        Time.timeScale = 0;
        foreach (var item in level)
        {
            SetActive(item, false);
        }
        string levelName = SceneManager.GetActiveScene().name;
        switch (levelName)
        {
            case "Level1":
                SetActive(level[0]);
                break;
            case "Level2":
                SetActive(level[1]);
                break;
            case "Level3":
                SetActive(level[2]);
                break;
            case "Level4":
                SetActive(level[3]);
                break;
            case "BonusLevel1":
                SetActive(level[4]);
                break;
            case "BonusLevel2":
                SetActive(level[5]);
                break;
            case "BonusLevel3":
                SetActive(level[6]);
                break;
            case "BonusLevel4":
                SetActive(level[7]);
                break;
            default:
                break;
        }

        StartCoroutine(CloseWnd());
    }
    IEnumerator CloseWnd()
    {
        float start = Time.realtimeSinceStartup;
        while (Time.realtimeSinceStartup < start + 2.0f)
        {
            yield return null;
        }
        SetWindSate(false);
    }
    protected override void ClearWnd()
    {
        base.ClearWnd();
        //恢复时间流 
        Time.timeScale = 1;

        uIManager.gameWnd.SetWindSate();
    }
}
