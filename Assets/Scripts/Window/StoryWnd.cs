﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class StoryWnd :WindowRoot
{
    public bool isShowStory = true;
    public VideoPlayer videoPlayer;
    public VideoClip[] videos;
    
    // Start is called before the first frame update
    protected override void InitWnd()
    {
        base.InitWnd();
        videoPlayer.frame = 0;
        videoPlayer.clip = null;
        Play();
    }

    protected override void ClearWnd()
    {
        videoPlayer.frame = 0;
        videoPlayer.clip = null;
        videoPlayer.targetTexture.Release();
        base.ClearWnd();
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            videoPlayer.time += 5;
            if (videoPlayer.time> videoPlayer.clip.length)
            {
                videoPlayer.time = videoPlayer.clip.length;
            }
        }
    }
    void EndVideo(VideoPlayer video)
    {
        //在视频结束时会调用这个函数

        LevelManager.Instance.NextScence();
        SetWindSate(false);
        Debug.Log("视频播放结束");
    }
    public void Play()
    {
        if (isShowStory)
        {
            switch (LevelManager.Instance.GetActiveSceneName())
            {
                case "Menu":

                    //Play BGM
                    AudioManager.Instance.PlayBGMusic("G1", false);

                    videoPlayer.clip = videos[0];
                    break;
                case "Level1":

                    //Play BGM
                    AudioManager.Instance.PlayBGMusic("G2", false);

                    videoPlayer.clip = videos[1];
                    break;
                case "Level2":

                    //Play BGM
                    AudioManager.Instance.PlayBGMusic("G3", false);

                    videoPlayer.clip = videos[2];
                    break;
                case "Level3":

                    //Play BGM
                    AudioManager.Instance.PlayBGMusic("G4", false);

                    videoPlayer.clip = videos[3];
                    break;
                case "Level4":
                    if (valueManager.GetPlayer1Score()> valueManager.GetPlayer2Score())
                    {
                        //Play BGM
                        AudioManager.Instance.PlayBGMusic("G5", false);

                        videoPlayer.clip = videos[4];
                    }
                    else if(valueManager.GetPlayer1Score() < valueManager.GetPlayer2Score())
                    {
                        //Play BGM
                        AudioManager.Instance.PlayBGMusic("G6", false);

                        videoPlayer.clip = videos[5];
                    }
                    isShowStory = false;
                    valueManager.isLocked = false;
                    break;
                default:
                    break;
            }
            videoPlayer.Play();
            videoPlayer.loopPointReached += EndVideo;
        }
        else
        {
            levelManager.NextScence();
            SetWindSate(false);
        }
        
    }
}
