﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// 结算界面
/// </summary>
public class CheckoutWnd : WindowRoot
{
    /// <summary>
    /// 使者进度条
    /// </summary>
    public Image imgFg1;
    /// <summary>
    /// 使者进度条指针
    /// </summary>
    public Image imgPoint1;
    /// <summary>
    /// 灾厄进度条
    /// </summary>
    public Image imgFg2;
    /// <summary>
    /// 灾厄进度条指针
    /// </summary>
    public Image imgPoint2;

    public float fgWidth1;
    private float fgWidth2;

    protected override void InitWnd()
    {
        Time.timeScale = 0;
        base.InitWnd();

        fgWidth1 = imgFg1.GetComponent<RectTransform>().sizeDelta.x;
        imgFg1.fillAmount = 0;
        imgPoint1.transform.localPosition = new Vector3(-fgWidth1 / 2, 100, 0);

        fgWidth2 = imgFg2.GetComponent<RectTransform>().sizeDelta.x;
        imgFg2.fillAmount = 0;
        imgPoint2.transform.localPosition = new Vector3(-fgWidth2 / 2, 80, 0);


        SetProgress1(valueManager.GetPlayer1Score() / 40);
        SetProgress2(valueManager.GetPlayer2Score() / 40);
    }

    protected override void ClearWnd()
    {
        Time.timeScale = 1;
        base.ClearWnd();
    }
    public void OnNextLevelBtnClick()
    {
        SetWindSate(false);
        uIManager.storyWnd.SetWindSate();
    }
    public void OnBackMenuBtnClick()
    {
        levelManager.LoadMenu();
        SetWindSate(false);
    }

    public void SetProgress1(float prg)
    {
        imgFg1.fillAmount = prg;

        float posX = prg * fgWidth1 - (fgWidth1 / 2);
        imgPoint1.GetComponent<RectTransform>().anchoredPosition = new Vector2(posX, 100);
    }

    public void SetProgress2(float prg)
    {
        imgFg2.fillAmount = prg;

        float posX = prg * fgWidth2 - (fgWidth2 / 2);
        imgPoint2.GetComponent<RectTransform>().anchoredPosition = new Vector2(posX, 80);
    }
}
