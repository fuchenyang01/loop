﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
public class GameWnd : WindowRoot 
{
    /// <summary>
    /// 占比图片
    /// </summary>
    [SerializeField]
    private Image imgFg;
    [SerializeField]
    private Image imgElpisCD;
    [SerializeField]
    private Image imgDisasterCD;
    [SerializeField]
    private Image imgOneDigit;
    [SerializeField]
    private Image[] imgTwoDigits;
    [SerializeField]
    private Image[] imgThreeDigits;
    [SerializeField]
    private GameObject OneDigit;
    [SerializeField]
    private GameObject TwoDigit;
    [SerializeField]
    private GameObject ThreeDigit;
    /// <summary>
    /// 技能加载完成后显示的图片
    /// </summary>
    [SerializeField]
    private GameObject imgElpisCDDone;
    /// <summary>
    /// 技能加载完成后显示的图片
    /// </summary>
    [SerializeField]
    private GameObject imgDisasterCDDone;
    /// <summary>
    /// 倒计时图片
    /// </summary>
    [SerializeField]
    private Sprite[] NumberSprites;
    /// <summary>
    /// 游戏开始前15秒放置道具的选项
    /// </summary>
    public Toggle[] toggles;
    /// <summary>
    /// 当前选中的按钮
    /// </summary>
    public GameObject activeToggles = null;
    /// <summary>
    /// 摆放的道具
    /// </summary>
    public GameObject[] tools;
    /// <summary>
    /// 玩家一技能读条面板
    /// </summary>
    [SerializeField]
    private GameObject Play1SkillPanal;
    /// <summary>
    /// 玩家二技能读条面板
    /// </summary>
    [SerializeField]
    private GameObject Play2SkillPanal;
    /// <summary>
    /// 道具选择面板
    /// </summary>
    [SerializeField]
    private GameObject ItemPanal;
    /// <summary>
    /// 对战开始
    /// </summary>
    [SerializeField]
    private GameObject tip1;
    /// <summary>
    /// 灾厄先行
    /// </summary>
    [SerializeField]
    private GameObject tip2;
    /// <summary>
    /// 游戏倒计时
    /// </summary>
    public int TotalTime = 180;
    /// <summary>
    /// 荆棘数量图片
    /// </summary>
    [SerializeField]
    private Sprite[] ImgJingjiHighlightDigits;
    [SerializeField]
    private Sprite[] ImgJingjiDisabledDigits;
    /// <summary>
    /// 荆棘数量图片GameObject
    /// </summary>
    [SerializeField]
    private Image GoJingjiDigit;


    protected override void InitWnd()
    {
        base.InitWnd();
        Play1SkillPanal.SetActive(false);
        Play2SkillPanal.SetActive(false);
        ItemPanal.SetActive(true);
        imgFg.fillAmount = 0;
        StartCoroutine(ShowTip2());

        OnValueChanged();
        
        foreach(Toggle toggle in toggles)
        {
            toggle.interactable = true;
        }
        GoJingjiDigit.sprite = ImgJingjiDisabledDigits[5];
        ////默认激活的道具
        //toggles[0].isOn = true;
    }
    protected override void ClearWnd()
    {

    }
    /// <summary>
    /// 更新占比
    /// </summary>
    /// <param name="grassCount"></param>
    /// <param name="wastelandCount"></param>
    public void UpdateProportion(int grassCount, int wastelandCount)
    {
        imgFg.fillAmount = (float)grassCount / ((float)wastelandCount + (float)grassCount);
    }
    /// <summary>
    /// 更新使者技能CD
    /// </summary>
    /// <param name="time"></param>
    public void UpdateTimeElpisCD(float time)
    {
        imgElpisCD.fillAmount = 1 - time / 10;
        if (imgElpisCD.fillAmount==1)
        {
            SetActive(imgElpisCDDone);
        }
        else
        {
            SetActive(imgElpisCDDone, false);
        }
    }
    /// <summary>
    /// 更新灾厄技能CD
    /// </summary>
    /// <param name="time"></param>
    public void UpdateTimeDisasterCD(float time)
    {
        imgDisasterCD.fillAmount = 1 - time / 15;

        if (imgDisasterCD.fillAmount == 1)
        {
            SetActive(imgDisasterCDDone);
        }
        else
        {
            SetActive(imgDisasterCDDone, false);
        }
    }

    public void OnSetBtnClick()
    {
        UIManager.Instance.setWnd.SetWindSate(true);
    }
    /// <summary>
    /// 更新时间
    /// </summary>
    /// <param name="time"></param>
    public void UpdateTimer(int time)
    {
        int unitPlace = time / 1 % 10;
        int tenPlace = time / 10 % 10;
        int hundredPlace = time / 100 % 10;
        //如果是三位数
        if (time >= 100)
        {
            SetActive(ThreeDigit);
            SetActive(TwoDigit, false);
            SetActive(OneDigit, false);
            imgThreeDigits[0].sprite = NumberSprites[unitPlace];
            imgThreeDigits[1].sprite = NumberSprites[tenPlace];
            imgThreeDigits[2].sprite = NumberSprites[hundredPlace];
        }
        //如果是两位数
        else if (time >= 10 && time < 100)
        {
            SetActive(ThreeDigit, false);
            SetActive(TwoDigit);
            SetActive(OneDigit, false);
            imgTwoDigits[0].sprite = NumberSprites[unitPlace];
            imgTwoDigits[1].sprite = NumberSprites[tenPlace];
        }
        //如果是一位数
        else if (time < 10 && time >= 0) 
        {
            SetActive(ThreeDigit, false);
            SetActive(TwoDigit, false);
            SetActive(OneDigit);
            imgOneDigit.sprite = NumberSprites[unitPlace];
        }
    }
    /// <summary>
    /// 倒计时
    /// </summary>
    /// <returns></returns>
    IEnumerator CountDown()
    {
        while (TotalTime >= 0)
        {
            UpdateTimer(TotalTime);
            yield return new WaitForSeconds(1);
            TotalTime--;
        }
        //时间结束
        //使者获胜
         if (imgFg.fillAmount>0.5)
         {
            valueManager.Play1Win();
         }
         //灾厄获胜
         else
         {
            valueManager.Play2Win();
         }

        valueManager.UpdatePlayer1Score((int)(imgFg.fillAmount * 10));
        valueManager.UpdatePlayer2Score(10 - valueManager.GetPlayer1Score());


        SetWindSate(false);
        uIManager.checkoutWnd.SetWindSate();
    }

    /// <summary>
    /// 放置道具倒计时
    /// </summary>
    /// <returns></returns>
    IEnumerator ItemCountDown()
    {
        float start = Time.realtimeSinceStartup;
        int timer=30; 
        while (timer >= 0)
        {
            UpdateTimer(timer);
            yield return new WaitForSeconds(1);
            timer--;
        }
        //时间结束,正式开始游戏
        Play1SkillPanal.SetActive(true);
        Play2SkillPanal.SetActive(true);
        ItemPanal.SetActive(false);
        StartCoroutine(ShowTip1());
    }
    IEnumerator ShowTip1()
    {
        tip1.SetActive(true);
        float start = Time.realtimeSinceStartup;
        while (Time.realtimeSinceStartup < start + 2.0f)
        {
            yield return null;
        }
        //关闭
        tip1.SetActive(false);
        StartCoroutine(CountDown());
    }

    IEnumerator ShowTip2()
    {
        tip2.SetActive(true);
        float start = Time.realtimeSinceStartup;
        while (Time.realtimeSinceStartup < start + 2.0f)
        {
            yield return null;
        }

        //关闭
        tip2.SetActive(false);
        StartCoroutine(ItemCountDown());
    }

    //放置道具
    public void OnValueChanged()
    {
        GameObject go = null;
        for (int i=0;i<toggles.GetLength(0);i++)
        {
            
            if (toggles[i].isOn == true)
            {
                go = GameObject.Find(tools[i].name + "(Clone)");
                if(!go)
                {
                    Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    go = Instantiate(tools[i], new Vector3(pos.x, pos.y, 0), Quaternion.identity, GameObject.Find(LevelManager.Instance.GetActiveSceneName()).transform.parent);

                }

                switch (i)
                {
                    case 0:
                        go.GetComponent<Spider>().isSetPos = true;
                        break;
                    case 1:
                        go.GetComponent<WindFlower>().isSetPos = true;
                        break;
                    case 2:
                        go.GetComponent<Bird>().isSetPos = true;
                        break;
                    case 3:
                        go.transform.position = Vector3.zero;
                        go.GetComponent<BramblesCreatorController>().Creator();
                        break;
                }
            }
            else
            {
                go = GameObject.Find(tools[i].name + "(Clone)");
                if(go!=null)
                {
                    DetectIsSetPos(go, i);
                }
            }
            
        }
    }


    //检测其他道具是否在放置
    void DetectIsSetPos(GameObject toggle, int index)
    {
        switch (index)
        {
            case 0:
                if (toggle.GetComponent<Spider>().isSetPos == true)
                {
                    Debug.Log(toggle.name);
                    Destroy(toggle);
                    toggles[index].interactable = true;
                }
                break;
            case 1:
                if (toggle.GetComponent<WindFlower>().isSetPos == true)
                {
                    Debug.Log(toggle.name);
                    Destroy(toggle);
                    toggles[index].interactable = true;
                }
                break;
            case 2:
                if (toggle.GetComponent<Bird>().isSetPos == true)
                {
                    Destroy(toggle);
                    toggles[index].interactable = true;
                }
                break;
            case 3:
                foreach (Transform child in toggle.transform)
                {
                    if (child.GetComponent<BramblesCreator>().isSetPos == true)
                    {
                        toggle.GetComponent<BramblesCreatorController>().ChangeNum(false);
                        if(toggle.transform.childCount == 1)
                        {
                            Destroy(toggle);
                        }
                        else
                        {
                            Destroy(child.gameObject);
                        }
                        toggles[index].interactable = true;
                    }
                }
                break;
            default:
                break;
        }
    }


    //改变荆棘数量图片
    public void ChangeJingjiDigit(int num,bool isHighlight)
    {
        if(num == 0)
        {
            GoJingjiDigit.gameObject.SetActive(false);
        }
        else
        {
            if (isHighlight)
            {
                GoJingjiDigit.sprite = ImgJingjiHighlightDigits[num - 1];
            }
            else
            {
                GoJingjiDigit.sprite = ImgJingjiDisabledDigits[num - 1];
            }
        }
        
    }
}