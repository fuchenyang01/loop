﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
public class NameListWnd : WindowRoot
{
    public VideoPlayer videoPlayer;
    protected override void InitWnd()
    {
        base.InitWnd();
        videoPlayer.Play();
        videoPlayer.loopPointReached += EndVideo;
    }
    protected override void ClearWnd()
    {
        base.ClearWnd();
        videoPlayer.targetTexture.Release();
    }

    void EndVideo(VideoPlayer video)
    {
        UIManager.Instance.menuWnd.SetWindSate();
        SetWindSate(false);
        Debug.Log("视频播放结束");
    }
}
