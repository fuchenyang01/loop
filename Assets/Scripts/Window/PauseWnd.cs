﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// 暂停界面
/// </summary>
public class PauseWnd : WindowRoot
{
    protected override void InitWnd()
    {
        Time.timeScale = 0;
        base.InitWnd();
    }

    protected override void ClearWnd()
    {
        base.ClearWnd();
        Time.timeScale = 1;
    }

    public void OnContinueBtnClick()
    {
        SetWindSate(false);
    }

    public void OnControlBtnClick()
    {
        uIManager.controlWnd.SetWindSate();
    }
    public void OnSoundBtnClick()
    {
        uIManager.setWnd.SetWindSate();
    }
    public void OnBackBtnClick()
    {
        SetWindSate(false);
        LevelManager.Instance.LoadMenu();
    }
}
