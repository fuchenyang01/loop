﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChooseWnd : WindowRoot
{
    //选择关卡界面
    [SerializeField]
    private GameObject chooseLevelPannal;
    [SerializeField]
    private Button btnStoryMode;
    [SerializeField]
    private Button btnJiejiMode;
    [SerializeField]
    private InputField time;
    [SerializeField]
    private GameObject[] levelNames;
    [SerializeField]
    private Toggle[] levels;
    private int activeLevel = 0;
    protected override void InitWnd()
    {
        base.InitWnd();
        levels[4].interactable = false;
        levels[5].interactable = false;
        levels[6].interactable = false;
        levels[7].interactable = false;

        if (valueManager.GetPlayer1Wincount()>=5)
        {
            levels[4].interactable = true;
        }
        if (valueManager.GetPlayer2Wincount() >= 5)
        {
            levels[5].interactable = true;
        }
        if (ValueManager.Instance.GetPlayer1Wincount() >= 10 && ValueManager.Instance.GetPlayer2Wincount() >= 10)
        {
            levels[6].interactable = true;
        }
        if (ValueManager.Instance.GetPlayer1Wincount() >= 20 && ValueManager.Instance.GetPlayer2Wincount() >= 20)
        {
            levels[7].interactable = true;
        }


        SetActive(btnStoryMode.gameObject);
        SetActive(btnJiejiMode.gameObject);
        chooseLevelPannal.SetActive(false);
        btnJiejiMode.interactable = !ValueManager.Instance.isLocked;
        time.text = "100";
    
    }

    protected override void ClearWnd()
    {
        base.ClearWnd();
    }

    public void OnStoryModeBtnClick()
    {
        //levelManager.LoadLevel1();
        SetWindSate(false);
        uIManager.storyWnd.SetWindSate();
    }
    public void OnStartBtnClick()
    {
       int gameTime = int.Parse(time.text);
        switch (activeLevel)
        {
            case 1:
                levelManager.LoadLevel1(gameTime);
                break;
            case 2:
                levelManager.LoadLevel2(gameTime);
                break;
            case 3:
                levelManager.LoadLevel3(gameTime);
                break;
            case 4:
                levelManager.LoadLevel4(gameTime);
                break;
            case 5:
                levelManager.LoadBonusLevel1(gameTime);
                break;
            case 6:
                levelManager.LoadBonusLevel2(gameTime);
                break;
            case 7:
                levelManager.LoadBonusLevel3(gameTime);
                break;
            case 8:
                levelManager.LoadBonusLevel4(gameTime);
                break;
            case 9:
                //随机
                LoadRandLevel(gameTime);
                break;
            default:
                LoadRandLevel(gameTime);
                break;
        }

        SetWindSate(false);
       
    }
    private void LoadRandLevel(int time)
    {
        int rand = Random.Range(1, 9);
        switch (rand)
        {
            case 1:
                levelManager.LoadLevel1(time);
                break;
            case 2:
                levelManager.LoadLevel2(time);
                break;
            case 3:
                levelManager.LoadLevel3(time);
                break;
            case 4:
                levelManager.LoadLevel4(time);
                break;
            case 5:
                if (levels[4].interactable==true)
                {
                    levelManager.LoadBonusLevel1(time);
                }
                else
                {
                    LoadRandLevel(time);
                }
                break;
            case 6:
                if (levels[5].interactable == true)
                {
                    levelManager.LoadBonusLevel2(time);
                }
                else
                {
                    LoadRandLevel(time);
                }
                break;
            case 7:
                if (levels[6].interactable == true)
                {
                    levelManager.LoadBonusLevel3(time);
                }
                else
                {
                    LoadRandLevel(time);
                }
                break;
            case 8:
                if (levels[7].interactable == true)
                {
                    levelManager.LoadBonusLevel4(time);
                }
                else
                {
                    LoadRandLevel(time);
                }
                break;
        }
    }
    public void OnJiejiModeBtnClick()
    {
        SetActive(btnStoryMode.gameObject, false);
        SetActive(btnJiejiMode.gameObject, false);
        chooseLevelPannal.SetActive(true);
    }
    public void OnBackMenuBtnClick()
    {
        uIManager.menuWnd.SetWindSate(true);
        SetWindSate(false);
    }

    public void OnValueChanged()
    {
        for (int i = 0; i < levels.GetLength(0); i++)
        {
            if (levels[i].isOn == true)
            {
                activeLevel = i+1;
                levelNames[i].SetActive(true);
            }
            else
            {
                levelNames[i].SetActive(false);
            }
        }
    }

    public void OnAchievementBtnClick()
    {
        UIManager.Instance.achievementWnd.SetWindSate(true);
        SetWindSate(false);
    }
}
