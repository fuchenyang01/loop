﻿using UnityEngine;
using UnityEngine.UI;

public class MenuWnd : WindowRoot 
{

    protected override void InitWnd()
    {
        base.InitWnd();

    }
    protected override void ClearWnd()
    {
        base.ClearWnd();

    }

    public void OnStartBtnClick()
    {
        UIManager.Instance.chooseWnd.SetWindSate(true);
        SetWindSate(false);
        //levelManager.LoadLevel1();
    }
    public void OnControlBtnClick()
    {
        UIManager.Instance.controlWnd.SetWindSate(true);
    }
    public void OnExitBtnClick()
    {
        #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
        #elif UNITY_STANDALONE_WIN
        Application.Quit();
        #endif
    }
    public void OnAudioSetBtnClick()
    {
        UIManager.Instance.setWnd.SetWindSate(true);
    }
    public void OnNameListBtnClick()
    {
        UIManager.Instance.nameListWnd.SetWindSate(true);
        SetWindSate(false);
    }
}