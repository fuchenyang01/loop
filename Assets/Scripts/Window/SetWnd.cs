﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetWnd : WindowRoot
{
    public Slider slider_Music;
    public Slider slider_Sound;


    protected override void InitWnd()
    {
        base.InitWnd();

        slider_Music.value = AudioManager.Instance.bgAudio.volume;
        slider_Sound.value = AudioManager.Instance.uiAudio.volume;
    }
    protected override void ClearWnd()
    {
        base.ClearWnd();

    }

    public void Slider_MusicOnValueChange()
    {
        AudioManager.Instance.bgAudio.volume = slider_Music.value;
    }
    public void slider_SoundOnValueChange()
    {
        AudioManager.Instance.uiAudio.volume = slider_Sound.value;
    }
    public void OnBackBtnClick()
    {
        this.SetWindSate(false);
    }
}
