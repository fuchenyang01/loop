﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingWnd : WindowRoot
{
    public Text txtTips;
    public Image imgFg;
    public Image imgPoint;
    public Text txtPrg;

    private float fgWidth;

    protected override void InitWnd()
    {
        base.InitWnd();

        fgWidth = imgFg.GetComponent<RectTransform>().sizeDelta.x;

        SetText(txtTips, "这是一条游戏Tips");
        SetText(txtPrg, "0%");
        imgFg.fillAmount = 0;
        imgPoint.transform.localPosition = new Vector3(-fgWidth / 2, 0, 0);
    }

    public void SetProgress(float prg)
    {
        SetText(txtPrg, (int)(prg * 100) + "%");
        imgFg.fillAmount = prg;

        float posX = prg * fgWidth - (fgWidth / 2);
        imgPoint.GetComponent<RectTransform>().anchoredPosition = new Vector2(posX, 0);
    }
}
