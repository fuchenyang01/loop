﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// 成就界面
/// </summary>
public class AchievementWnd : WindowRoot
{
    [SerializeField]
    private Image[] Levels;
    /// <summary>
    /// 0 白色
    /// 1 紫色
    /// 2 白紫
    /// </summary>
    [SerializeField]
    private Sprite[] unlock;
    /// <summary>
    /// 使者路径
    /// </summary>
    [SerializeField]
    private GameObject path1;
    /// <summary>
    /// 灾厄路径
    /// </summary>
    [SerializeField]
    private GameObject path2;
    protected override void InitWnd()
    {
        base.InitWnd();
        for (int i = 0; i < path1.transform.childCount; i++)
        {
            path1.transform.GetChild(i).gameObject.SetActive(false);
        }
        for (int i = 0; i < path2.transform.childCount; i++)
        {
            path2.transform.GetChild(i).gameObject.SetActive(false);
        }

        if (ValueManager.Instance.GetPlayer1Wincount()<=20)
        {
            for (int i = 0; i < ValueManager.Instance.GetPlayer1Wincount(); i++)
            {
                path1.transform.GetChild(i).gameObject.SetActive(true);
            }
        }
        else
        {
            for (int i = 0; i < 20; i++)
            {
                path1.transform.GetChild(i).gameObject.SetActive(true);
            }
        }

        if (ValueManager.Instance.GetPlayer2Wincount() <= 20)
        {
            for (int i = 0; i < ValueManager.Instance.GetPlayer2Wincount(); i++)
            {
                path2.transform.GetChild(i).gameObject.SetActive(true);
            }
        }
        else
        {
            for (int i = 0; i < 20; i++)
            {
                path2.transform.GetChild(i).gameObject.SetActive(true);
            }
        }
        if (ValueManager.Instance.GetPlayer1Wincount() >= 5)
        {
            Levels[0].sprite = unlock[0];
        }
        if (ValueManager.Instance.GetPlayer2Wincount() >= 5)
        {
            Levels[1].sprite = unlock[1];
        }
        if (ValueManager.Instance.GetPlayer1Wincount() >= 10&& ValueManager.Instance.GetPlayer2Wincount() >= 10)
        {
            Levels[2].sprite = unlock[2];
        }
        if (ValueManager.Instance.GetPlayer1Wincount() >= 20 && ValueManager.Instance.GetPlayer2Wincount() >= 20)
        {
            Levels[3].sprite = unlock[2];
        }
    }

    protected override void ClearWnd()
    {
        base.ClearWnd();
    }

    public void OnBackMenuBtnClick()
    {
        uIManager.menuWnd.SetWindSate(true);
        SetWindSate(false);
    }
}
