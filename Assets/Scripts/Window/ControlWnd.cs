﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// 控制界面
/// </summary>
public class ControlWnd : WindowRoot
{

    protected override void InitWnd()
    {
        base.InitWnd();
    }

    protected override void ClearWnd()
    {
        base.ClearWnd();
    }

    public void OnBackBtnClick()
    {
        this.SetWindSate(false);
        //uIManager.menuWnd.SetWindSate();
    }
}
