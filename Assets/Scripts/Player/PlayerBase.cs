﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBase : MonoBehaviour
{

    #region private variable
    /// <summary>
    /// 使者地面的sprite
    /// </summary>
    private Sprite[] sprite_elpis;
    /// <summary>
    /// 灾厄地面的sprite
    /// </summary>
    private Sprite[] sprite_disaster;
    /// <summary>
    /// 玩家状态
    /// </summary>
    private PlayerState state;
    /// <summary>
    /// 使者-灾厄特效
    /// </summary>
    private GameObject ParticalS2Z;
    /// <summary>
    /// 灾厄-使者特效
    /// </summary>
    private GameObject ParticalZ2S;

    #endregion

    #region public variable
    /// <summary>
    /// 碰撞体组件
    /// </summary>
    public Collider2D coll;
    /// <summary>
    /// 地图layer
    /// </summary>
    public LayerMask map;
    /// <summary>
    /// 检测地面物体
    /// </summary>
    public Transform GroundCheck;
    /// <summary>
    /// 移动速度
    /// </summary>
    public float movSpeed;
    /// <summary>
    /// 跳跃高度
    /// </summary>
    public float jumpForce;
    /// <summary>
    /// 多段跳次数
    /// </summary>
    public int jumpCount = 2;
    
    #endregion

    #region projected variable
    /// <summary>
    /// Rigidbody组件
    /// </summary>
    protected Rigidbody2D rBody;
    /// <summary>
    /// Animator组件
    /// </summary>
    protected Animator anim;
    /// <summary>
    /// 是否在地面上
    /// </summary>
    [SerializeField]
    protected bool isGround;
    /// <summary>
    /// 是否在跳跃
    /// </summary>
    [SerializeField]
    protected bool isJump;
    /// <summary>
    /// 是否按下跳跃键
    /// </summary>
    protected bool jumpPressed = false;
    /// <summary>
    /// 关卡基类的引用
    /// </summary>
    protected LevelBase levelBase;
    /// <summary>
    /// 是否在绿株上，用于跳跃
    /// </summary>
    protected bool isOnGP;
    /// <summary>
    /// 是否技能在冷却 
    /// </summary>
    protected bool isSkillCD;
    /// <summary>
    /// 技能cd
    /// </summary>
    protected float timeSkillCD;
    /// <summary>
    /// 存储初始状态 [movSpeed, jumpForce, isSkillCD]
    /// </summary>
    protected float[] initState;
    /// <summary>
    /// 是否有Debuff
    /// </summary>
    protected bool isDebuff;
    /// <summary>
    /// Debuff 持续时间
    /// </summary>
    protected float timeDebuffLast;
    /// <summary>
    /// 是否可以释放技能
    /// </summary>
    protected bool isCanReleaseSkill = true;
    /// <summary>
    /// Audio Source 组件
    /// </summary>
    protected AudioSource AS;
    #endregion


    // Start is called before the first frame update

    void Awake()
    {
        string level = LevelManager.Instance.GetActiveSceneName();
        switch (level)
        {
            case "Level1":
                sprite_elpis = GameObject.FindGameObjectWithTag("Level").GetComponent<Level1>().sprite_elpis;
                sprite_disaster = GameObject.FindGameObjectWithTag("Level").GetComponent<Level1>().sprite_disaster;
                ParticalS2Z = GameObject.FindGameObjectWithTag("Level").GetComponent<Level1>().particalS2Z;
                ParticalZ2S = GameObject.FindGameObjectWithTag("Level").GetComponent<Level1>().particalZ2S;
                break;
            case "Level2":
                sprite_elpis = GameObject.FindGameObjectWithTag("Level").GetComponent<Level2>().sprite_elpis;
                sprite_disaster = GameObject.FindGameObjectWithTag("Level").GetComponent<Level2>().sprite_disaster;
                ParticalS2Z = GameObject.FindGameObjectWithTag("Level").GetComponent<Level2>().particalS2Z;
                ParticalZ2S = GameObject.FindGameObjectWithTag("Level").GetComponent<Level2>().particalZ2S;
                break;
            case "Level3":
                sprite_elpis = GameObject.FindGameObjectWithTag("Level").GetComponent<Level3>().sprite_elpis;
                sprite_disaster = GameObject.FindGameObjectWithTag("Level").GetComponent<Level3>().sprite_disaster;
                ParticalS2Z = GameObject.FindGameObjectWithTag("Level").GetComponent<Level3>().particalS2Z;
                ParticalZ2S = GameObject.FindGameObjectWithTag("Level").GetComponent<Level3>().particalZ2S;
                break;
            case "Level4":
                sprite_elpis = GameObject.FindGameObjectWithTag("Level").GetComponent<Level4>().sprite_elpis;
                sprite_disaster = GameObject.FindGameObjectWithTag("Level").GetComponent<Level4>().sprite_disaster;
                ParticalS2Z = GameObject.FindGameObjectWithTag("Level").GetComponent<Level4>().particalS2Z;
                ParticalZ2S = GameObject.FindGameObjectWithTag("Level").GetComponent<Level4>().particalZ2S;
                break;
            case "BonusLevel1":
                sprite_elpis = GameObject.FindGameObjectWithTag("Level").GetComponent<BonusLevel1>().sprite_elpis;
                sprite_disaster = GameObject.FindGameObjectWithTag("Level").GetComponent<BonusLevel1>().sprite_disaster;
                ParticalS2Z = GameObject.FindGameObjectWithTag("Level").GetComponent<BonusLevel1>().particalS2Z;
                ParticalZ2S = GameObject.FindGameObjectWithTag("Level").GetComponent<BonusLevel1>().particalZ2S;
                break;
            case "BonusLevel2":
                sprite_elpis = GameObject.FindGameObjectWithTag("Level").GetComponent<BonusLevel2>().sprite_elpis;
                sprite_disaster = GameObject.FindGameObjectWithTag("Level").GetComponent<BonusLevel2>().sprite_disaster;
                ParticalS2Z = GameObject.FindGameObjectWithTag("Level").GetComponent<BonusLevel2>().particalS2Z;
                ParticalZ2S = GameObject.FindGameObjectWithTag("Level").GetComponent<BonusLevel2>().particalZ2S;
                break;
            case "BonusLevel3":
                sprite_elpis = GameObject.FindGameObjectWithTag("Level").GetComponent<BonusLevel3>().sprite_elpis;
                sprite_disaster = GameObject.FindGameObjectWithTag("Level").GetComponent<BonusLevel3>().sprite_disaster;
                ParticalS2Z = GameObject.FindGameObjectWithTag("Level").GetComponent<BonusLevel3>().particalS2Z;
                ParticalZ2S = GameObject.FindGameObjectWithTag("Level").GetComponent<BonusLevel3>().particalZ2S;
                break;
            case "BonusLevel4":
                sprite_elpis = GameObject.FindGameObjectWithTag("Level").GetComponent<BonusLevel4>().sprite_elpis;
                sprite_disaster = GameObject.FindGameObjectWithTag("Level").GetComponent<BonusLevel4>().sprite_disaster;
                ParticalS2Z = GameObject.FindGameObjectWithTag("Level").GetComponent<BonusLevel4>().particalS2Z;
                ParticalZ2S = GameObject.FindGameObjectWithTag("Level").GetComponent<BonusLevel4>().particalZ2S;
                break;
            default:
                break;
        }

    }

    // Update is called once per frame
    void Update()
    {

    }

    //角色移动
    //playerID is 1 : 1p; playerID is 2 : 2p
    protected void Movement(int playerID)
    {
        float movX;
        if (playerID == 1)
        {
            movX = Input.GetAxisRaw("Horizontal");
            rBody.velocity = new Vector2(movX * movSpeed, rBody.velocity.y);
        }
        else if (playerID == 2)
        {
            movX = Input.GetAxisRaw("HorizontalExtra");
            rBody.velocity = new Vector2(movX * movSpeed, rBody.velocity.y);//速度为1.1倍
        }
        else
        {
            movX = 0f;
        }



        if (movX != 0)
        {
            transform.localScale = new Vector3(movX, 1, 1) * Mathf.Abs(transform.localScale.x);

            //if (playerID == 1)
            //{
            //    AudioManager.Instance.PlayUIAudio("Elpis_move");
            //}
            //else
            //{
            //    AudioManager.Instance.PlayUIAudio("Disaster_move");
            //}

        }

    }

    //角色跳跃
    protected void Jump(int playerID)
    {
        //多段跳跃
        if (isGround)
        {
            jumpCount = 2;
            isJump = false;
        }

        if (jumpPressed && isGround)
        {
            isJump = true;
            rBody.velocity = new Vector2(rBody.velocity.x, jumpForce);
            jumpCount--;

            jumpPressed = false;
            if(playerID == 1)
            {
                PlayControlAudio("Elpis_jump");
            }
            else
            {
                PlayControlAudio("Disaster_jump");
            }
        }
        if (jumpPressed && jumpCount > 0 && isJump)
        {
            rBody.velocity = new Vector2(rBody.velocity.x, jumpForce);
            jumpCount--;

            jumpPressed = false;
            if (playerID == 1)
            {
                PlayControlAudio("Elpis_jump");
            }
            else
            {
                PlayControlAudio("Disaster_jump");
            }
        }


    }

    //切换动画效果
    protected void SwitchAnim(int playerID)
    {
        //anim.SetFloat("running", Mathf.Abs(rBody.velocity.x));
        
        if (isGround)
        {
            anim.SetBool("jumping", false);
            anim.SetBool("falling", false);
        }
        else if (!isGround && rBody.velocity.y > 0)
        {
            anim.SetBool("jumping", true);
        }
        else if (rBody.velocity.y <= 0)
        {
            anim.SetBool("jumping", false);
            anim.SetBool("falling", true);
        }
        

        if(playerID == 1)
        {
            if (state == PlayerState.SpeedDown)
            {
                anim.SetBool("speedDowning", true);
            }
            else
            {
                anim.SetBool("speedDowning", false);
            }

            if (state == PlayerState.Dizzy)
            {
                anim.SetBool("dizzy", true);
            }
            else
            {
                anim.SetBool("dizzy", false);
            }


            if (state == PlayerState.Imprison)
            {
                anim.SetBool("imprison", true);
            }
            else
            {
                anim.SetBool("imprison", false);
            }
        }
    }

    //碰撞触发器
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Collection")
        {
            Destroy(collision.gameObject);
        }
    }

    //检测方块
    //playerID is 1 : 1p; playerID is 2 : 2p
    protected void DetectGround(int playerID)
    {

        RaycastHit2D hit = Physics2D.Raycast(GroundCheck.position, Vector2.down, 0.9f, 
            1<<LayerMask.NameToLayer("Elpis") | 1<<LayerMask.NameToLayer("Disaster") | 1<<LayerMask.NameToLayer("map"));
        
        if (hit)
        {
            //踩在玩家上
            if (hit.transform.gameObject.layer == LayerMask.NameToLayer("Elpis") ||
                hit.transform.gameObject.layer == LayerMask.NameToLayer("Disaster"))
            {
                jumpCount = 1;
                return;
            }
            if (hit.transform.gameObject.layer == LayerMask.NameToLayer("map"))
            {
                if (playerID == 1)
                {
                    if (hit.transform.tag == "Disaster")
                    {
                        if (levelBase.wasteland.Contains(hit.collider.gameObject))
                        {
                            levelBase.wasteland.Remove(hit.collider.gameObject);
                            levelBase.grass.Add(hit.collider.gameObject);
                        }
                        int ran = Random.Range(0, sprite_elpis.Length);
                        hit.transform.GetComponent<SpriteRenderer>().sprite = sprite_elpis[ran];
                        hit.transform.tag = "Elpis";
                        GameObject go = Instantiate(ParticalZ2S, hit.transform);
                        go.GetComponent<ParticleSystem>().Play();
                        //更新占比
                        UIManager.Instance.gameWnd.UpdateProportion(levelBase.grass.Count, levelBase.wasteland.Count);
                    }
                }
                else if (playerID == 2)
                {
                    if (hit.transform.tag == "Elpis")
                    {
                        if (levelBase.grass.Contains(hit.collider.gameObject))
                        {
                            levelBase.grass.Remove(hit.collider.gameObject);
                            levelBase.wasteland.Add(hit.collider.gameObject);
                        }
                        int ran = Random.Range(0, sprite_disaster.Length);
                        hit.transform.GetComponent<SpriteRenderer>().sprite = sprite_disaster[ran];
                        hit.transform.tag = "Disaster";
                        GameObject go = Instantiate(ParticalS2Z, hit.transform);
                        go.GetComponent<ParticleSystem>().Play();
                        //更新占比
                        UIManager.Instance.gameWnd.UpdateProportion(levelBase.grass.Count, levelBase.wasteland.Count);
                    }
                }
            }

        }
    }

    //检测越界
    protected void DetectOverFlow()
    {
        int floorPosX = (int)transform.localPosition.x;
        int floorPosY = (int)transform.localPosition.y;

        //x方向速度界限设置为0f时有bug：慢慢出地图会从上方掉落
        //y方向速度小于10不是向下落出地图
        RightOverFlow(floorPosX, floorPosY);
        LeftOverFlow(floorPosX, floorPosY);
        DownOverFlow(floorPosX, floorPosY);
        UpOverFlow(floorPosX, floorPosY);
    }

    //地图右侧越界
    void RightOverFlow(int floorPosX, int floorPosY)
    {
        if (transform.localPosition.x > 47f)
        {
            if (rBody.velocity.y > -10f)
            {
                //检测另一侧是否堵住
                if (floorPosY != 27 && levelBase.levelMap[27 - floorPosY - 1, 0] == 0)
                {
                    transform.localPosition = new Vector3(0, transform.localPosition.y, 0);
                }
                else
                {
                    transform.localPosition = new Vector3(floorPosX, transform.localPosition.y, 0);
                }
            }
        }
    }

    //地图左侧越界
    void LeftOverFlow(int floorPosX, int floorPosY)
    {
        if (transform.localPosition.x < 0f)
        {
            if (rBody.velocity.y > -10f)
            {
                //检测另一侧是否堵住
                if (floorPosY != 27 && levelBase.levelMap[27 - floorPosY - 1, 47] == 0)
                {
                    transform.localPosition = new Vector3(47, transform.localPosition.y, 0);
                }
                else
                {
                    transform.localPosition = new Vector3(floorPosX, transform.localPosition.y, 0);
                }
            }
        }
    }

    //地图下侧越界
    void DownOverFlow(int floorPosX, int floorPosY)
    {
        if (transform.localPosition.y < 0f)
        {
            if (rBody.velocity.y < 10f)
            {
                transform.localPosition = new Vector3(transform.localPosition.x, 26, 0);
            }
        }
    }

    //地图上侧越界
    void UpOverFlow(int floorPosX, int floorPosY)
    {
        
        if (transform.localPosition.y > 27f)
        {
            if (rBody.velocity.y > 9f)
            {
                //检测另一侧是否堵住
                if (floorPosX != 47 && levelBase.levelMap[26, floorPosX] == 0)
                {
                    transform.localPosition = new Vector3(transform.localPosition.x, 0, 0);
                }
                else
                {
                    transform.localPosition = new Vector3(transform.localPosition.x, floorPosY, 0);
                }
                
            }
        }
    }

    //切换状态
    public void SwitchState(PlayerState state)
    {
        this.state = state;
        switch (state)
        {
            case PlayerState.Dizzy:
                resetState();

                movSpeed = 0f;
                jumpForce = 0f;
                
                isCanReleaseSkill = false;

                isDebuff = true;
                timeDebuffLast = 1f;
                break;
            case PlayerState.Imprison:
                resetState();

                movSpeed = 0f;
                jumpForce = 0f;

                isDebuff = true;
                timeDebuffLast = 1f;
                break;
            case PlayerState.SpeedDown:
                resetState();

                movSpeed *= 0.4f;
                break;
            case PlayerState.SpeedUp:
                resetState();

                movSpeed *= 1.5f;
                break;
            case PlayerState.Normal:
                resetState();
                break;
        }
    }

    void resetState()
    {
        movSpeed = initState[0];
        jumpForce = initState[1];
        isCanReleaseSkill = true;
    }

    /// <summary>
    /// 角色控制音效
    /// </summary>
    public void PlayControlAudio(string name)
    {
        AudioClip audio = ResManager.Instance.LoadAudio("Audio/SoundEffect/" + name, true);
        AS.clip = audio;
        AS.Play();
    }
}
