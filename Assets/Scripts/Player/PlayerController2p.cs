﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController2p : PlayerBase
{
    /// <summary>
    /// 是否停止松开enter，用于延时
    /// </summary>
    private bool isSkillStop = false;
    /// <summary>
    /// 是否在使用技能，用于pc1p检测是否使用技能时被撞到
    /// </summary>
    public bool Skilling = false;

    // Start is called before the first frame update
    void Start()
    {
        //state reset
        movSpeed = 5.5f;
        jumpForce = 15f;
        initState = new float[2] {movSpeed, jumpForce};

        isSkillCD = false;
        timeSkillCD = 18f;

        rBody = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        levelBase = GameObject.FindGameObjectWithTag("Level").GetComponent<LevelBase>();
        AS = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        //检测jump
        if (Input.GetButtonDown("JumpExtra")&&jumpCount>0)
        {
            jumpPressed = true;
        }

        // 计时器——10s(灾厄技能冷却)
        TimerSkillCD();
    }

    void FixedUpdate()
    {
        //检测是否在地面上
        isGround = Physics2D.OverlapCircle(GroundCheck.position, 0.2f, map);

        Movement(2);
        SwitchAnim(2);
        Jump(2);

        //检测方块
        DetectGround(2);

        //检测越界
        DetectOverFlow();
        
        //释放技能
        SkillRelease();
    }
    
    void SkillRelease()
    {
        if ((Input.GetKey(KeyCode.KeypadEnter) || Input.GetKey(KeyCode.Return)) && !isSkillCD && isCanReleaseSkill)
        {
            //直接计时15s
            isSkillCD = true;
            timeSkillCD = 18f;
        }
        //延时0.1s，速度50，移动5格
        if(isSkillCD && !isSkillStop)
        {
            Skilling = true;
            if (transform.localScale.x > 0)
            {
                rBody.velocity = new Vector2(15f, rBody.velocity.y);
            }
            else if(transform.localScale.x < 0)
            {
                rBody.velocity = new Vector2(-15f, rBody.velocity.y);
            }
            anim.SetTrigger("skilling");
            PlayControlAudio("Disaster_skill");
        }
        else
        {
            Skilling = false;
        }
    }

    //技能cd计时器
    void TimerSkillCD()
    {
        if (isSkillCD)
        {
            timeSkillCD -= Time.deltaTime;
            UIManager.Instance.gameWnd.UpdateTimeDisasterCD(timeSkillCD);
            //延时0.1s停止按键盘
            if (timeSkillCD <= 17.8f)
            {
                isSkillStop = true;
            }
            if (timeSkillCD <= 0)
            {
                isSkillStop = false;
                isSkillCD = false;
            }
        }
    }

}
