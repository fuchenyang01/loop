﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class PlayerController1p : PlayerBase
{
    //使者技能释放的墙面素材
    [SerializeField]
    private GameObject skill_wall;
    //使者技能产生的墙面整体
    private GameObject skill_go;
    

    // Start is called before the first frame update
    void Start()
    {
        //state reset
        movSpeed = 5f;
        jumpForce = 15f;
        initState = new float[2] {movSpeed, jumpForce};

        isSkillCD = false;
        timeSkillCD = 10f;

        rBody = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        levelBase = GameObject.FindGameObjectWithTag("Level").GetComponent<LevelBase>();
        AS = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

        //检测jump
        if (Input.GetButtonDown("Jump") && jumpCount > 0)
        {
            jumpPressed = true;
        }

        // 计时器——10s(使者技能冷却)
        TimerSkillCD();

        TimerDebuff();
    }

    void FixedUpdate()
    {
        //检测是否在地面上
        isGround = Physics2D.OverlapCircle(GroundCheck.position, 0.2f, map);

        Movement(1);
        SwitchAnim(1);
        Jump(1);

        //检测方块
        DetectGround(1);
        //检测越界
        DetectOverFlow();

        //释放技能
        SkillRelease();
        
    }

    //检测碰撞到P2玩家
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.layer == LayerMask.NameToLayer("Disaster"))
        { 
            PlayerController2p pc2p;
            if (collision.gameObject.TryGetComponent<PlayerController2p>(out pc2p))
            {
                if (pc2p.Skilling)
                {
                    SwitchState(PlayerState.Dizzy);
                }
            }  
        }
    }

    //释放技能
    void SkillRelease()
    {
        //在地面上
        if (Input.GetKey(KeyCode.Space) && isGround && !isSkillCD && isCanReleaseSkill)
        {
            int floorPosX = Mathf.RoundToInt(transform.localPosition.x);
            int floorPosY = Mathf.RoundToInt(transform.localPosition.y);
            SkillCreateWall(floorPosX, floorPosY);

        }
    }

    //释放技能创造墙
    void SkillCreateWall(int posX, int posY)
    {
        anim.SetTrigger("skilling");
        skill_go = new GameObject("WallBySkill");
        skill_go.transform.localPosition = new Vector3(posX, posY, 0);
        skill_go.transform.SetParent(transform.parent);
        skill_go.layer = 9; // "Elpis"

        GameObject wall1 = Instantiate(skill_wall, skill_go.transform);
        wall1.layer = 9; // "Elpis"
        if (levelBase.levelMap[27 - posY - 2, posX] == 0)
        {
            GameObject wall2 = Instantiate(skill_wall, skill_go.transform);
            wall2.transform.localPosition = new Vector3(0, 1, 0);
            wall2.layer = 9; // "Elpis"
            if (levelBase.levelMap[27 - posY - 3, posX] == 0)
            {
                GameObject wall3 = Instantiate(skill_wall, skill_go.transform);
                wall3.transform.localPosition = new Vector3(0, 2, 0);
                wall3.layer = 9; // "Elpis"
                if (levelBase.levelMap[27 - posY - 4, posX] == 0)
                {
                    GameObject wall4 = Instantiate(skill_wall, skill_go.transform);
                    wall4.transform.localPosition = new Vector3(0, 3, 0);
                    wall4.layer = 9; // "Elpis"
                }
            }
        }

        isSkillCD = true;
        timeSkillCD = 10f;
        PlayControlAudio("Elpis_skill");
    }

    //进入绿株
    public void StayGP()
    {
        anim.SetBool("climbing", true);
        //rBody.gravityScale = 1;
        if(Input.GetButton("Jump"))
        {
            rBody.velocity = new Vector2(rBody.velocity.x, 10f);
        }
        else
        {
            rBody.velocity = new Vector2(rBody.velocity.x, -0.2f);
        }
        
        isOnGP = true;
    }

    //离开绿株
    public void LeaveGP()
    {
        anim.SetBool("climbing", false);
        //rBody.gravityScale = 5;
        isOnGP = false;
    }

    //踩生石花
    public void EnterLithops()
    {
        rBody.velocity = new Vector2(rBody.velocity.x, 25f);
        jumpCount = 1;
        isJump = true;
    }


    #region Timer
    //技能cd计时器
    void TimerSkillCD()
    {
        if (isSkillCD)
        {
            timeSkillCD -= Time.deltaTime;
            UIManager.Instance.gameWnd.UpdateTimeElpisCD(timeSkillCD);

            if (timeSkillCD <= 6)
            {
                Destroy(skill_go);
            }
            if (timeSkillCD <= 0)
            {
                isSkillCD = false;
            }
        }
    }

    //Debuff计时器
    void TimerDebuff()
    {
        if(isDebuff)
        {
            timeDebuffLast -= Time.deltaTime;
            if(timeDebuffLast<=0)
            {
                isDebuff = false;
                SwitchState(PlayerState.Normal);
            }
        }
    }
    #endregion
}
