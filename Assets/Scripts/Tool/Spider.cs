﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spider : MonoBehaviour
{
    public GameObject SpiderThread;

    float timer = 2f;
    bool isFall;

    public bool isSetPos = false;

    private LevelBase levelBase;

    // Start is called before the first frame update
    void Start()
    {
        isFall = true;
        Instantiate(SpiderThread,this.transform);
        levelBase = GameObject.FindGameObjectWithTag("Level").GetComponent<LevelBase>();
    }

    // Update is called once per frame
    void Update()
    {
        if(isFall)
        {
            timer -= Time.deltaTime;
            if(timer<=0f)
            {
                timer = 2f;
                Instantiate(SpiderThread,this.transform);
            }
        }

        if(isSetPos)
        {
            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            this.transform.position = new Vector3(Mathf.Round(pos.x), Mathf.Round(pos.y) + 0.5f, 0);

            if(Mathf.RoundToInt(pos.y)>25 || Mathf.RoundToInt(pos.x)>47)
            {
                return;
            }
            if(Input.GetMouseButtonDown(0))
            {
                if (levelBase.levelMap[27 - Mathf.RoundToInt(pos.y) - 2, Mathf.RoundToInt(pos.x)] != 0
                    && levelBase.levelMap[27 - Mathf.RoundToInt(pos.y) - 1, Mathf.RoundToInt(pos.x)] == 0)
                {
                    UIManager.Instance.gameWnd.toggles[0].interactable = false;
                    isSetPos = false;
                }
            }
        }
    }
}
