﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brambles : MonoBehaviour
{
    
    

    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 9)
        {
            AudioManager.Instance.PlayToolAudio("Mechanism_jingji");
            collision.gameObject.GetComponent<PlayerController1p>().SwitchState(PlayerState.SpeedDown);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 9)
        {
            collision.gameObject.GetComponent<PlayerController1p>().SwitchState(PlayerState.Normal);
        }
    }
}
