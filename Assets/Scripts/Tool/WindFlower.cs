﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindFlower : MonoBehaviour
{
    public bool isSetPos = false;

    private LevelBase levelBase;

    // Start is called before the first frame update
    void Start()
    {
        levelBase = GameObject.FindGameObjectWithTag("Level").GetComponent<LevelBase>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isSetPos)
        {
            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            this.transform.position = new Vector3(Mathf.RoundToInt(pos.x), Mathf.RoundToInt(pos.y) - 0.5f, 0);

            if (Mathf.RoundToInt(pos.y) > 25 || Mathf.RoundToInt(pos.x) > 47)
            {
                return;
            }

            if (Input.GetMouseButtonDown(0))
            {
                if (levelBase.levelMap[27 - Mathf.RoundToInt(pos.y), Mathf.RoundToInt(pos.x)] != 0
                    && levelBase.levelMap[27 - Mathf.RoundToInt(pos.y) - 1, Mathf.RoundToInt(pos.x)] == 0
                    && levelBase.levelMap[27 - Mathf.RoundToInt(pos.y) - 2, Mathf.RoundToInt(pos.x)] == 0)
                {
                    UIManager.Instance.gameWnd.toggles[1].interactable = false;
                    isSetPos = false;
                }
            }
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                this.transform.localScale = new Vector3(-1, 1, 1);
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                this.transform.localScale = new Vector3(1, 1, 1);
            }

            if (Input.GetMouseButtonDown(1))
            {
                if (this.transform.localScale == new Vector3(1, 1, 1))
                {
                    this.transform.localScale = new Vector3(-1, 1, 1);
                }
                else
                {
                    this.transform.localScale = new Vector3(1, 1, 1);
                }
            }
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        AudioManager.Instance.PlayToolAudio("Mechanism_fengmohua");
        collision.GetComponent<Rigidbody2D>().AddForce(new Vector2(80 *transform.localScale.x, 0));
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        AudioManager.Instance.PauseToolAudio("Mechanism_fengmohua");
    }
}
