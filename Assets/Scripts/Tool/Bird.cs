﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : MonoBehaviour
{
    public bool isSetPos = false;

    private LevelBase levelBase;

    // Start is called before the first frame update
    void Start()
    {
        levelBase = GameObject.FindGameObjectWithTag("Level").GetComponent<LevelBase>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isSetPos)
        {
            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            this.transform.position = new Vector3(Mathf.Round(pos.x), Mathf.Round(pos.y) + 0.5f, 0);

            if (Mathf.RoundToInt(pos.y) > 25 || Mathf.RoundToInt(pos.x) > 47)
            {
                return;
            }
            if (Input.GetMouseButtonDown(0))
            {
                if(levelBase.levelMap[27 - Mathf.RoundToInt(pos.y) - 2, Mathf.RoundToInt(pos.x)] != 0
                     && levelBase.levelMap[27 - Mathf.RoundToInt(pos.y) - 1, Mathf.RoundToInt(pos.x)] == 0)
                {
                    UIManager.Instance.gameWnd.toggles[2].interactable = false;
                    isSetPos = false;
                }
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 9)
        {
            collision.gameObject.GetComponent<PlayerController1p>().SwitchState(PlayerState.Dizzy);

            AudioManager.Instance.PlayToolAudio("Mechanism_wujiaoniao");
        }
        
    }
}
