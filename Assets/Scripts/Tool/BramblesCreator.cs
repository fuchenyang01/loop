﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BramblesCreator : MonoBehaviour
{
    public bool isSetPos = false;
    private LevelBase levelBase;
    // Start is called before the first frame update
    void Start()
    {
        levelBase = GameObject.FindGameObjectWithTag("Level").GetComponent<LevelBase>();
    }

    // Update is called once per frame
    void Update()
    {
        SetPos();
    }

    public void SetPos()
    {
        if(isSetPos)
        {
            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            this.transform.position = new Vector3(Mathf.RoundToInt(pos.x), Mathf.RoundToInt(pos.y) - 0.25f, 0);

            if (Mathf.RoundToInt(pos.y) > 25 || Mathf.RoundToInt(pos.x) > 47)
            {
                return;
            }

            Vector2 trans_pos = new Vector2(Mathf.RoundToInt(pos.x), Mathf.RoundToInt(pos.y));
            if (Input.GetMouseButtonDown(0))
            {
                if (levelBase.levelMap[27 - Mathf.RoundToInt(pos.y), Mathf.RoundToInt(pos.x)] != 0
                    && levelBase.levelMap[27 - Mathf.RoundToInt(pos.y) - 1, Mathf.RoundToInt(pos.x)] == 0
                    && !isPosInList(trans_pos))
                {
                    isSetPos = false;
                    this.transform.parent.GetComponent<BramblesCreatorController>().SetSuccess(trans_pos);

                }
            }
        }
        
    }

    bool isPosInList(Vector2 pos)
    {
        if (this.transform.parent.GetComponent<BramblesCreatorController>().BramblePosList.Contains(pos))
            return true;
        else
            return false;
    }
}
