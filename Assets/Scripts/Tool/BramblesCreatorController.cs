﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BramblesCreatorController : MonoBehaviour
{
    
    private int Bramble_num = 6;
    public List<Vector2> BramblePosList;
    // Start is called before the first frame update
    void Start()
    {
        Bramble_num = 6;
        BramblePosList = new List<Vector2>(6);
    }

    // Update is called once per frame
    void Update()
    {
        if (Bramble_num == 0)
        {
            UIManager.Instance.gameWnd.activeToggles = null;
            UIManager.Instance.gameWnd.toggles[3].interactable = false;
        }
    }

    public void Creator()
    {
        UIManager.Instance.gameWnd.ChangeJingjiDigit(Bramble_num, true);
        this.transform.GetChild(0).GetComponent<BramblesCreator>().isSetPos = true ;
    }

    public void SetSuccess(Vector2 pos)
    {
        
        Bramble_num--;
        BramblePosList.Add(pos);
        UIManager.Instance.gameWnd.ChangeJingjiDigit(Bramble_num, true);
        UIManager.Instance.gameWnd.toggles[3].Select();
        if(Bramble_num > 0)
        {
            GameObject go = Instantiate(this.transform.GetChild(0).gameObject, this.transform);
            go.GetComponent<BramblesCreator>().isSetPos = true;
        }
        
    }

    public void ChangeNum(bool isHighlight)
    {
        UIManager.Instance.gameWnd.ChangeJingjiDigit(Bramble_num, isHighlight);
    }
}
