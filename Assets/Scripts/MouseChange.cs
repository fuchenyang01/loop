﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MouseChange : MonoBehaviour
{
    public Texture2D texture;//自己想要的鼠标图片
    public Texture2D texture2;//自己想要的鼠标图片


    private void Update()
    {
        if (IsPointerOverGameObject(Input.mousePosition))
        {
            Cursor.SetCursor(texture2, Vector2.zero, CursorMode.Auto);
        }
        else
        {
            Cursor.SetCursor(texture, Vector2.zero, CursorMode.Auto);
        }
        
    }
    private bool IsPointerOverGameObject(Vector2 mousePosition)
    {
        //创建一个点击事件
        PointerEventData eventData = new PointerEventData(EventSystem.current);
        eventData.position = mousePosition;
        List<RaycastResult> raycastResults = new List<RaycastResult>();
        //向点击位置发射一条射线，检测是否点击UI
        EventSystem.current.RaycastAll(eventData, raycastResults);
        if (raycastResults.Count > 0)
        {
            for (int i = raycastResults.Count - 1; i >= 0; i--)
            {
                if (!raycastResults[i].gameObject.GetComponent<UnityEngine.UI.Button>())
                    raycastResults.RemoveAt(i);

            }
            return raycastResults.Count > 0;
        }
        else
        {
            return false;
        }
    }

}
